package com.telmendev.school.weatherlab;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {
    private String TAG = MainActivity.class.getSimpleName();
    private ListView lv;
    private static String API_URL = "http://api.openweathermap.org/data/2.5/weather?APPID=e889467f1fe2caab564f218af3201e63&units=metric";
    private Button search,xmlparsing;
    private EditText cityName;
    public TextView tvTemp,tvCity,tvDesc;
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        search = (Button) findViewById(R.id.btnJsonNow);
        xmlparsing = (Button) findViewById(R.id.btnXmlForecast);
        cityName = (EditText) findViewById(R.id.editText);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!cityName.getText().toString().matches("")) {
                    new JsonTask().execute(API_URL, cityName.getText().toString());
                }
                else {
                    String toastmsg = "Хотын нэр оруулна !!";
                    Toast.makeText(MainActivity.this, toastmsg, Toast.LENGTH_SHORT).show();
                }

            }
        });
        xmlparsing.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!cityName.getText().toString().matches("")) {
                    Intent intent = new Intent(MainActivity.this,XMLParsing.class);
                    intent.putExtra("city_name",cityName.getText().toString());
                    startActivity(intent);
                }
                else {
                    String toastmsg = "Хотын нэр оруулна !!";
                    Toast.makeText(MainActivity.this, toastmsg, Toast.LENGTH_SHORT).show();
                }
            }
        });
        tvTemp = (TextView) findViewById(R.id.tvTemp);
        tvCity = (TextView) findViewById(R.id.tvCity);
        tvDesc = (TextView) findViewById(R.id.tvDesc);
    }
    private class JsonTask extends AsyncTask<String,String,String> {
        protected void onPreExecute() {
            super.onPreExecute();

            pd = new ProgressDialog(MainActivity.this);
            pd.setMessage("Please wait");
            pd.setCancelable(false);
            pd.show();
        }
        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]+"&q="+params[1]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line+"\n");
                    Log.i("Response:", " "+ line);
                }
                return buffer.toString();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (pd.isShowing()){
                pd.dismiss();
            }
            if (result != "") {
                try {
                    JSONObject jsobj = new JSONObject(result);
                    JSONArray jsarr = jsobj.getJSONArray("weather");
                    JSONObject jsobject = jsobj.getJSONObject("main");
                    tvTemp.setText(String.valueOf(jsobject.getDouble("temp")));
                    tvCity.setText(jsobj.getString("name"));
                    tvDesc.setText(jsarr.getJSONObject(0).getString("description"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        }
    }
}
