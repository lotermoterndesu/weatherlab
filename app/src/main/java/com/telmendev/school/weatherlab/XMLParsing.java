package com.telmendev.school.weatherlab;

import android.app.ListActivity;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class XMLParsing extends ListActivity {
    private String cityName;
//    private Button btnback;
    private static String XML_URL = "http://api.openweathermap.org/data/2.5/forecast/daily?mode=xml&units=metric&cnt=7&APPID=e889467f1fe2caab564f218af3201e63";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_xmlparsing);
        cityName = getIntent().getStringExtra("city_name");
//        btnback = (Button) findViewById(R.id.btnBack);
//        btnback.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(XMLParsing.this,MainActivity.class);
//                startActivity(intent);
//            }
//        });
        new GetForecast().execute(XML_URL, cityName);
    }
    private class GetForecast extends AsyncTask<String,String,String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Toast.makeText(XMLParsing.this,"XML data is downloading",Toast.LENGTH_LONG).show();

        }
        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]+"&q="+params[1]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line+"\n");
                }
                return buffer.toString();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.i("SDA", result);
            ArrayList<HashMap<String, String>> listItems = new ArrayList<HashMap<String, String>>();
            Document doc = getDOMelement(result);
            NodeList nl = doc.getElementsByTagName("time");
            Log.i("NODESDA", String.valueOf((Element) nl.item(0)));
            for(int i = 0; i<nl.getLength();i++) {
                HashMap<String,String> map = new HashMap<>();
                Element e = (Element)nl.item(i);
                map.put("temperature", "Celsius" + getValue(e, "temperature"));
                listItems.add(map);
            }
            ListAdapter adapter = new SimpleAdapter(XMLParsing.this, listItems, R.layout.list_item, new String[] {"temperature"}, new int[] {R.id.temp});
            setListAdapter(adapter);
        }
    }
    public Document getDOMelement(String xml) {
        Document doc = null;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml));
            doc = db.parse(is);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
            return null;
        } catch (SAXException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return doc;
    }
    public String getValue(Element  item, String str) {
        NodeList n = item.getElementsByTagName(str);
        Log.i("SDAA", String.valueOf(item.getNextSibling()));
        return getElementValue(n.item(0));
    }

    public final String getElementValue(Node item) {
        Node child;
        if (item != null) {
            if (item.hasChildNodes()) {
                for (child = item.getFirstChild(); child != null; child = child.getNextSibling()) {
                    Log.i("SDAA", String.valueOf(item.getAttributes().getNamedItem("day")));
                    if (child.getNodeType() == Node.TEXT_NODE) {
                        return child.getNodeValue();
                    }
                }
            }
        }
        return "";
    }
}
